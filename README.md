<p align="center">
  <img src="https://i.pinimg.com/originals/c8/d7/a2/c8d7a24c1e2b9af95bf0fce6313aa95d.jpg"
  width=200/>
</p>
<h1 align="center">Welcome to MarvelComics App 📖</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-0.0.1-blue.svg?cacheSeconds=2592000" />
</p>

> MarvelComics is a mobile application to seek your favorites marvel characters and their comics. This app was created with React Native using MarvelComics API. 

Searching for your favorite character           |  Choosing a character to view your comics
:-------------------------:|:-------------------------:
![Characters Screen](documentation/AppScreenshot1.gif)  |  ![Comics Screen](documentation/AppScreenshot2.gif)


## Functionalities
- Infinity scrollable characters;
- An input to filter the characters list;
- Get all the comics from a specific character; 



## Technologies and libs
- React Native 0.64.1;
- TypeScript;
- Styled Components;
- React Navigation
- React Native Vector Icons
- Axios
- React Native Video


For testing purpose:
- [Jest](https://jestjs.io/pt-BR/) - a simple framework to test JavaScript code;
- [React Native Testing Library](https://testing-library.com/docs/react-native-testing-library/intro/) - a testing library to test React Native components;
- [React Hooks Testing Library](https://github.com/testing-library/react-hooks-testing-library) - another testing library to test React hooks;

<p align="center">
<img src="https://wallpapercave.com/wp/wp1829364.jpg"  height=300 />
</p>

## Installation
To run this app you need to satisfy these requirements below:

### Requirements
- Git;
- Node.JS;
- Yarn;
- Android Studio;
- JDK 11;
- Emulator (optional) - you can emulate an Android device using Android Studio (AVD Manager);

If you need help to prepare your development environment I recommend this complete [guide](https://react-native.rocketseat.dev/) created by RocketSeat.

### Download and Install 
1. To download this repository to your machine use this command:
```git
git clone https://github.com/lucasmoura-dev/marvep-app.git
```
2. A folder `marvel-app` will be created with app content, open it and execute the command below to download all dependencies (this step may take a while):
```sh
yarn
```

### Changing the API Key
By default this app contains a limited API Key from Marvel Comics that has a request limit (max. 3000 request/day), if you want to change it you can modify the configuration file on this [site](https://developer.marvel.com/ "site").

![Set the Marvel API key](documentation/SetMarvelApiKey.png)  

## How to run
Execute this command to run this app for Android

```sh
yarn android
```

You can run this app for iOS but it hasn't been tested yet and may not work perfectly
```
yarn  ios
```

## Run tests
If you wan't to test the components, screens and React Hooks you just need to execute this command:

```sh
yarn test
```

## Author

👤 **Lucas Moura**

* Github: [@lucasmoura-dev](https://github.com/lucasmoura-dev)
* LinkedIn: [@lucas-moura-251951120](https://linkedin.com/in/lucas-moura-251951120)

