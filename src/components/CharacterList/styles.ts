import colors from '@styles/colors';
import styled from 'styled-components/native';

export const Container = styled.View`
  width: 100%;
  height: 450px;
`;

export const ErrorMessage = styled.Text`
  color: ${colors.lightGrey};
  text-align: center;
  font-size: 18px;
`;