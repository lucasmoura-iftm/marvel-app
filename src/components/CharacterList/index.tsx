import React, {useCallback} from 'react';
import { FlatList, ListRenderItemInfo } from 'react-native';
import { MarvelCharacter } from '~/@types/marvel';

import { Container, ErrorMessage } from './styles';
import CharacterCard from '@components/CharacterCard';

type ChracterListProps = {
  characters: MarvelCharacter[];
  fetchNextCharacters: () => void;
  loading: boolean;
}

const ChracterList: React.FC<ChracterListProps> = ({ characters, fetchNextCharacters, loading }) => {
  const renderItem = useCallback(({item, index}: ListRenderItemInfo<MarvelCharacter>) => {
    return <CharacterCard character={item} index={index} />
  }, [characters]);

  return (
    <Container>
      { characters.length === 0 && !loading &&
        <ErrorMessage>Nenhum personagem foi encontrado.</ErrorMessage>
      }
      <FlatList
        data={characters}
        renderItem={renderItem}
        onEndReachedThreshold={0.6}
        numColumns={1}
        horizontal={false}
        showsVerticalScrollIndicator={false}
        onEndReached={() => fetchNextCharacters()}
        keyExtractor={character => character.id.toString()}
        testID="characters-flatlist"
      />
    </Container>
  );
};

export default ChracterList;
