import styled from 'styled-components/native';

import colors from '@styles/colors';

export const Container = styled.View`
  flex: 1;
  border-radius: 10px;
  align-items: center;
  background: grey;
  height: 50px;
`;

export const ComicWrapper = styled.TouchableOpacity`
  width: 150px; 
  height: 290px; 
  flex-direction: column; 
  align-items: center; 
  margin-bottom: 30px;
`;

export const ComicCoverWrapper = styled.View`
  width: 150px;
  height: 200px;
  background: #444448;
`;

export const ComicCover = styled.Image`
  flex: 1;
  resize-mode: stretch;
`;

export const ComicOverlay = styled.View`
  position: absolute;
  bottom: 15px;
  right: 0;
  background: #dc143c;
  z-index: 1;
  padding: 5px;
  max-width: 120px;
  border-top-left-radius: 2px;
  border-bottom-left-radius: 2px;
`;

export const IssueNumber = styled.Text`
  color: #fff;
  font-size: 16px;
`;

export const ComicFooter = styled.View`
  flex: 1;
  align-items: center;
  padding: 10px 0;
`;

export const ComicTitle = styled.Text`
  color: #fff;
  text-align: center;
  font-size: 18px;
  font-weight: bold;
`;

export const ComicPrice = styled.Text`
  color: #ffbe0b;
  margin-top: 5px;
  font-size: 18px;
`;
