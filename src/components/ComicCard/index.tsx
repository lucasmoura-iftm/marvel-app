import React, {memo} from 'react';
import { ComicCover, ComicWrapper, ComicCoverWrapper, ComicOverlay, IssueNumber, ComicFooter, ComicPrice, ComicTitle } from './styles';

import { MarvelComic } from '~/@types/marvel';

type ComicCardProps = {
  comic: MarvelComic;
  index: number;
}

const ComicCard: React.FC<ComicCardProps> = ({ comic, index}) => {
  console.log('renderizando o comic de index ', index, comic.id);

  return (
    <ComicWrapper testID="comic-card">
      <ComicCoverWrapper>
        <ComicOverlay>
          <IssueNumber>{'#' + comic.issueNumber}</IssueNumber>
        </ComicOverlay>
        <ComicCover testID="comic-card-cover" source={{ uri: comic.cover }} />
      </ComicCoverWrapper>
      <ComicFooter>
        <ComicPrice>{'$' + comic.price}</ComicPrice>
        <ComicTitle numberOfLines={2}>{comic.title}</ComicTitle>
      </ComicFooter>
    </ComicWrapper>
  );
};

export default memo(ComicCard);
