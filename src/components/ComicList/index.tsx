import ComicCard from '@components/ComicCard';
import React, {useCallback} from 'react';
import {FlatList, ListRenderItemInfo} from 'react-native';
import { MarvelComic } from '~/@types/marvel';

import {Container, ErrorMessage} from './styles';

type ComicListProps = {
  comics: MarvelComic[];
  fetchNextComics: () => void;
  loading: boolean;
}

const ComicList: React.FC<ComicListProps> = ({ comics, fetchNextComics, loading }) => {

  const renderItem = useCallback(({item, index}: ListRenderItemInfo<MarvelComic>) => {
    return (
      <ComicCard comic={item} index={index} />
    );
  }, [comics]);

  return (
    <Container>
      { comics.length === 0 && !loading &&
        <ErrorMessage>Esse personagem não contém comics.</ErrorMessage>
      }
      <FlatList
        data={comics}
        renderItem={renderItem}
        onEndReachedThreshold={0.9}
        numColumns={2}
        horizontal={false}
        showsVerticalScrollIndicator={false}
        columnWrapperStyle={{
          justifyContent: 'space-between',
        }}
        onEndReached={() => fetchNextComics()}
        keyExtractor={comic => comic.id.toString()}
        testID="comics-flatlist"
      />
    </Container>
  );
};

export default ComicList;
