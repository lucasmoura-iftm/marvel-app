import { useNavigation } from '@react-navigation/native';
import React, { memo } from 'react';
import BookIcon from 'react-native-vector-icons/Ionicons';
import { MarvelCharacter } from '~/@types/marvel';

import { Bubble, ComicCover, ComicCoverWrapper, ComicWrapper, Name, NameContent, TotalComics, TotalComicsContent } from './styles';

type CharacterCardProps = {
  character: MarvelCharacter;
  index: number;
}

const CharacterCard: React.FC<CharacterCardProps> = ({ character, index }) => {
  const navigation = useNavigation();  
  console.log('renderizando o char de index ', index, character.id);

  return (
    <ComicWrapper testID="character-card" onPress={() => {
      navigation.navigate('CharacterComics', { character} );
    }}>
      <ComicCoverWrapper>
        <NameContent>
          <Name testID="character-card-name">{ character.name }</Name>
        </NameContent>
        <ComicCover testID="character-card-cover" source={{ uri: character.thumbnail }} />
        <TotalComicsContent>
          <Bubble />
          <BookIcon name="book-outline" size={20} />
          <TotalComics testID="character-card-total-comics">{ character.totalComics }</TotalComics>
        </TotalComicsContent>
      </ComicCoverWrapper>
    </ComicWrapper>
  );
}

export default memo(CharacterCard);