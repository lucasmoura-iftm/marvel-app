import colors from '@styles/colors';
import styled from 'styled-components/native';

export const ComicWrapper = styled.TouchableOpacity`
  flex: 1;
  height: 150px; 
  flex-shrink: 0;
  flex-direction: column; 
  align-items: center; 
  margin-bottom: 10px;
`;

export const ComicCoverWrapper = styled.View`
  width: 100%;
  height: 150px;
  flex-shrink: 0;
  background: #444448;
  border: solid 2px #000;
`;

export const NameContent = styled.View`
  position: absolute;
  bottom: 15px;
  left: 10px;
  background: #e3cf22;
  border: solid 2px black;
  z-index: 1;
  padding: 10px;
  max-width: 150px;
  border-top-left-radius: 2px;
  border-bottom-left-radius: 2px;
`;

export const Name = styled.Text`
  color: #4c3c00;
  font-family: 'Comic Sans';
  font-size: 16px;
  text-align: center;
`;

export const ComicCover = styled.Image`
  flex: 1;
  resize-mode: cover;
`;

export const TotalComicsContent = styled.View`
  position: absolute;
  border-radius: 20px;
  border: solid 2px #000;
  top: 15px;
  right: 10px;
  background: #fff;
  min-width: 30px;
  min-height: 50px;
  
  z-index: 1;
  padding: 5px 20px;
  max-width: 150px;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

export const Bubble = styled.View`
  position: absolute;
  border-radius: 15px;
  border: solid 2px #000;
  width: 18px;
  height: 18px;
  background: #fff;
  z-index: 1;
  bottom: -18px;
  right: -12px;
`;

export const TotalComics = styled(Name)`
  font-size: 16px;
  margin-left: 5px;
`;

export const ComicFooter = styled.View`
  justify-content: space-between;
  flex: 1;
  align-items: center;
  padding: 10px 0;
`;
