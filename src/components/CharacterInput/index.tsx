import colors from '@styles/colors';
import React, { useState } from 'react';

import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';

import { Container, TextInput } from './styles';

interface CharacterInputProps {
  onSubmit: (/*text: string*/) => void;
  value: string;
  onChangeText?: (text: string) => void;
  onClear: () => void;
}

const CharacterInput: React.FC<CharacterInputProps> = ({onSubmit, value, onChangeText, onClear}) => {
  const handleInputButton = () => {
    const canClearInput = value.length > 0;
    console.log('handleInputButton canClear?', canClearInput);
    if (canClearInput) {
      onClear();
      /*setText('');
      onSubmit('');*/
    }
  }

  return (
  <Container>
    <TextInput
      keyboardAppearance="dark"
      placeholderTextColor="#666360"
      placeholder="Procurando alguém? Nome?"
      returnKeyType="search"
      selectionColor={colors.purple}
      value={value}
      onChangeText={onChangeText/*text => setText(text)*/}
      onSubmitEditing={() => onSubmit()}
      testID="character-input"
    />
    <FontAwesomeIcon 
      name={value.length === 0 ? 'search' : 'close'} 
      size={30}
      onPress={handleInputButton}
      testID="character-input-button"
    />

  </Container>);
};
export default CharacterInput;
