import styled from 'styled-components/native';
import colors from '@styles/colors';

export const Container = styled.View`
  width: 100%;
  height: 60px;
  padding: 0 16px;
  background: ${colors.white};
  border: solid 2px #000;
  border-radius: 20px;
  margin-bottom: 8px;

  flex-direction: row;
  align-items: center;
`;

export const TextInput = styled.TextInput`
  flex: 1;
  color: #000;
  font-size: 17px;
`;
