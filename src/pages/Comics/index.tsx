import React, { useEffect } from 'react';
import BookIcon from 'react-native-vector-icons/Ionicons';
import { StyleSheet } from 'react-native';

import { StackNavigationProp } from '@react-navigation/stack';
import { CharacterBackground, CharacterImage, Container, Header, ComicsContent, HeaderTitle, Divider, ComicsHeader, TotalContent, Total } from './styles';
import ComicList from '@components/ComicList';
import colors from '@styles/colors';
import { useMarvelComics } from '../../hooks/marvelComics';

const Comics: React.FC = ({ navigation, route }) => {
  const { character } = route.params;
  const [comics, fetchComics, loading] = useMarvelComics(character.id);

  useEffect(() => {
    if (character && character.name) {
      navigation.setOptions({title: character.name});
    }
  }, []);

  const fetchNext = () => {
    fetchComics();
  }

  return (
  <Container>
    <Header>
      <CharacterBackground 
        source={{ uri: character.thumbnail }} 
        style={StyleSheet.absoluteFill}
        blurRadius={10}
      />
      <CharacterImage source={{ uri: character.thumbnail }} />
      <Divider />   
    </Header>

    <ComicsContent>
      <ComicsHeader>
        <HeaderTitle>Comics</HeaderTitle>
        <TotalContent>
          <BookIcon name="book-outline" size={20} color={colors.white} />
          <Total>{character.totalComics}</Total>
        </TotalContent>
      </ComicsHeader>
      
      <ComicList comics={comics} fetchNextComics={fetchNext} loading={loading} />
    </ComicsContent>
  </Container>
  );
}

export default Comics;