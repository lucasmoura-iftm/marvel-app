import React, { useState } from 'react';

import CharacterInput from '@components/CharacterInput';
import CharacterList from '@components/CharacterList';
import MarvelComicsLogo from '@assets/marvel-comics.png';

import { Container, Header, Logo, SearchBar } from './styles';
import { useCharacters } from '../../hooks/marvelCharacters';

const Home: React.FC = () => {
  const [characters, fetchCharacters, loading] = useCharacters(''); 
  const [characterName, setCharacterName] = useState('');

  const searchCharacter = (/*characterName: string*/) => {
    fetchCharacters(characterName, 0);
    // setCharacterName(characterName);
  };

  const fetchNext = () => {
    fetchCharacters(characterName);
  }

  const onClear = () => {
    setCharacterName('');
    fetchCharacters('', 0);
  }

  return <Container>
    <Header>
      <Logo source={MarvelComicsLogo}/>
    </Header>
    <SearchBar>
      <CharacterInput
        value={characterName}
        onClear={onClear} 
        onChangeText={text => setCharacterName(text)}
        onSubmit={searchCharacter}
      />
    </SearchBar>
    <CharacterList characters={characters} fetchNextCharacters={fetchNext} loading={loading} />
  </Container>
}

export default Home;