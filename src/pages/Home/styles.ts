import colors from '@styles/colors';
import styled from 'styled-components/native';


export const Container = styled.View`
  flex: 1;
  align-items: flex-start;
  padding: 40px 30px;
`;

export const Header = styled.View`
  flex-direction: row;
  width: 100%;
  justify-content: center;
  align-items: center;
`;

export const Logo = styled.Image`
  width: 150px;
  height: 123px;
`;

export const SearchBar = styled.View`
  width: 100%;
  margin: 10px 0;
  flex-direction: row;
`;

export const SearchInput = styled.TextInput`
  width: 100%;
  height: 50px;
  border-radius: 10px;
  background: ${colors.input};
`;