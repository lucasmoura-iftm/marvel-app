import { useNavigation } from '@react-navigation/native';
import Video from "react-native-video";

import React, { useRef } from 'react';
import { useEffect } from 'react';
import { View, Animated, StyleSheet, Text, Button, SafeAreaView, Dimensions } from 'react-native';

import MarvelSplash from '../../assets/marvel-splash.png';
import MarvelVideoIntro from '../../assets/video-marvel-intro.mp4';

import { AnimatedText, Container } from './styles';

const WIDTH = Dimensions.get('screen').width;
const HEIGHT = Dimensions.get('screen').height;

const Splash: React.FC = () => {
  const navigation = useNavigation(); 
  const splashFadeIn = useRef(new Animated.Value(0)).current;
  const textAnimations = [
    useRef(new Animated.Value(0)).current,
    useRef(new Animated.Value(0)).current,
    useRef(new Animated.Value(0)).current
  ];

  useEffect(() => {
    
    // playSequence();
  }, []);

  const playSequence = () => {
    const sequence = createTextAnimations();
    console.log(sequence);
    Animated.sequence(sequence).start(() => goHome());
  }

  const goHome = () => {
    navigation.navigate('Home');
  }

  const createTextAnimations = () : Animated.CompositeAnimation[] => {
    const sequence: Animated.CompositeAnimation[] = [createSplashAnimation()];
    textAnimations.forEach(target => {
      sequence.push(
        Animated.timing(target, {
          toValue: 1,
          duration: 800,
          useNativeDriver: true,
        }),
        Animated.delay(2000),
        Animated.timing(target, {
          toValue: 0,
          duration: 800,
          useNativeDriver: true,
        })
      );
    });
    return sequence;
  }

  const createSplashAnimation = () => {
    return Animated.timing(splashFadeIn, {
      toValue: 1,
      duration: 500,
      useNativeDriver: true,
    });
  }

  return (
    <Container>
      <Video
        source={MarvelVideoIntro}
        style={styles.backgroundVideo}
        muted={false}
        repeat={false}
        resizeMode={"cover"}
        rate={1.0}
        ignoreSilentSwitch={"obey"}
        onEnd={() => playSequence()}
        testID="background-video"
      />
      <Animated.Image source={MarvelSplash} style={ { position: 'absolute', width: WIDTH, height: HEIGHT, opacity: splashFadeIn }} />
      <Animated.Text style={[ styles.fadingText, { opacity: textAnimations[0] } ]}
      >
        Carregando uma tonelada de personagens do universo Marvel
      </Animated.Text>
      <Animated.Text style={[ styles.fadingText, { opacity: textAnimations[1] } ]}
      >
        Que você poderá escolher facilmente
      </Animated.Text>
      <Animated.Text style={[ styles.fadingText, { opacity: textAnimations[2] } ]}
      >
        Para encontrar todos os quadrinhos já publicados desse personagem
      </Animated.Text>
    </Container>
  );
}

const styles = StyleSheet.create({
  fadingText: {
    position: 'absolute', 
    color: '#fff', 
    fontSize: 18, 
    textAlign: 'center',
    bottom: 100,
  },
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  fadingContainer: {
    padding: 20,
    backgroundColor: "powderblue"
  },
  backgroundVideo: {
    height: HEIGHT,
    position: "absolute",
    top: 0,
    left: 0,
    alignItems: "stretch",
    bottom: 0,
    right: 0
  }
});

export default Splash;