import styled from 'styled-components/native';
import { Animated } from 'react-native';

export const Container = styled.SafeAreaView`
  justify-content: flex-end;
  align-items: center;
  flex: 1;
  padding: 25px;
`;

type AnimatedTextProps = {
  opacity: number;
}

export const AnimatedText = styled(Animated.Text)<AnimatedTextProps>`
  color: '#fff'; 
  opacity: ${p => p.opacity};
  font-size: 18;
  text-align: center;
`;