import { getCharacters, getComics } from "@services/marvelapi";
import { useCallback, useEffect, useState } from "react";
import { MarvelComic } from "~/@types/marvel";

interface ComicsApiResult {
  id: number;
  title: string;
  issueNumber: number;
  thumbnail: { path: string; extension: string };
  prices: any[];
}

const parseComics = (results: ComicsApiResult[]): MarvelComic[] => {
  const newComics = results.map(({ id, title: oldTitle, issueNumber, thumbnail, prices }) => {
    let cover = '';
    if (thumbnail) {
      cover = `${thumbnail.path}.${thumbnail.extension}`;
    }
    const price = prices.length > 0 ? prices[0].price : 0;
    const title = oldTitle.replace(` #${issueNumber}`, '');
    // console.log(`Novo ${issueNumber} - ${title}`);
    return { id, title, issueNumber, cover, price };
  });

  return newComics;
}

export const useMarvelComics = (characterId: number): [MarvelComic[], (offset?: number | null) => Promise<void>, boolean] => {
  const [loading, setLoading] = useState(false);
  const [comics, setComics] = useState<MarvelComic[]>([]);
  const [totalComics, setTotalComics] = useState(null);

  useEffect(() => {
    fetchComics(0);
  }, [characterId]);

  const fetchComics = async (offset: number | null = null) => {
    const canClearComicsList = offset === 0;
    if (totalComics === comics.length && !canClearComicsList) {
      return;
    }

    setLoading(true);
    
    const { data: { results, total }} = await getComics(offset ?? comics.length, characterId);
    const newComics = parseComics(results);

    
    if (canClearComicsList) {
      setComics([...newComics]);
    } else {
      setComics([...comics, ...newComics]);
    }
    
    setTotalComics(total);
    setLoading(false);

  };

  return [comics, fetchComics, loading];
}
