import React, { useContext, createContext, ReactNode, useState, useCallback } from 'react';

interface AuthProviderProps {
  children: ReactNode;
}

interface IAuthContextData {
  user: string;
  favoriteCharacter: string;
  addFavoriteCharacter: (characterName: string) => void;
  clearFavoriteCharacter: () => void;
}

const AuthContext = createContext<IAuthContextData>({} as IAuthContextData);

function AuthProvider({ children }: AuthProviderProps) {
  const [favoriteCharacter, setFavoriteCharacter] = useState<string>('');

  const addFavoriteCharacter = useCallback((characterName: string) => {
    setFavoriteCharacter(characterName);
  }, []);

  const clearFavoriteCharacter = useCallback(() => {
    setFavoriteCharacter('');
  }, []);

  return (
    <AuthContext.Provider value={{
      user: 'Lucas',
      favoriteCharacter,
      addFavoriteCharacter,
      clearFavoriteCharacter
    }}>
      { children }
    </AuthContext.Provider>
  );
}

function useAuth() {
  const context = useContext(AuthContext);
  return context;
}

export { AuthProvider, useAuth };