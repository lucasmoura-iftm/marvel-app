import { getCharacters, getComics } from "@services/marvelapi";
import { useCallback, useEffect, useState } from "react";
import { MarvelCharacter } from "~/@types/marvel";

interface CharactersApiResult {
  id: number;
  name: string;
  thumbnail: { path: string; extension: string };
  comics: { available: number; }
}

const parseCharacters = (results: CharactersApiResult[]): MarvelCharacter[] => {
  const newCharacters = results.map(({ id, name, thumbnail: thumbnailObject, comics }) => {
    const { available: totalComics  } = comics;
    let thumbnail = '';
    if (thumbnailObject && thumbnailObject.path && thumbnailObject.extension) {
      thumbnail = `${thumbnailObject.path}.${thumbnailObject.extension}`;
    }

    return { id, name, totalComics, thumbnail };
  });

  return newCharacters;
}

export const useCharacters = (characterName: string): [MarvelCharacter[], (characterName: string, offset?: number | null) => Promise<void>, boolean] => {
  const [loading, setLoading] = useState(false);
  const [characters, setCharacters] = useState<MarvelCharacter[]>([]);
  const [totalCharacters, setTotalCharacters] = useState(null);

  useEffect(() => {
    fetchCharacters(characterName, 0);
  }, [characterName]);

  const fetchCharacters = async (characterName: string, offset: number | null = null) => {
    const canClearCharactersList = offset === 0;
    if (totalCharacters === characters.length && !canClearCharactersList) {
      return;
    }

    setLoading(true);
    
    const { data: { results, total }} = await getCharacters(offset ?? characters.length, characterName);
    const newCharacters = parseCharacters(results);

    if (canClearCharactersList) {
      setCharacters([...newCharacters]);
    } else {
      setCharacters([...characters, ...newCharacters]);
    }
    
    setTotalCharacters(total);
    setLoading(false);

  };

  return [characters, fetchCharacters, loading];
}
