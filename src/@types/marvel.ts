export interface MarvelComic {
  id: number;
  title: string;
  issueNumber: number;
  cover: string;
  price: number;
}

export interface MarvelCharacter {
  id: number;
  name: string;
  totalComics: number;
  thumbnail: string;
}