import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import Splash from '../pages/Splash';
import Home from '../pages/Home';
import Comics from '../pages/Comics';

import colors from '../styles/colors';


const Stack = createStackNavigator();

const Routes: React.FC = () => (
  <Stack.Navigator
    screenOptions={{
      cardStyle: {backgroundColor: colors.background},
      headerTransparent: true,
      headerStyle: {
        backgroundColor: colors.background,
        shadowColor: 'transparent',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        textShadowOffset: {width: 2, height: 2},
        textShadowRadius: 5,
        textShadowColor: 'black',
      }
    }}>
    <Stack.Screen name="Splash" component={Splash} options={{headerShown: false}} />
    <Stack.Screen name="Home" component={Home} options={{headerShown: false}} />
    <Stack.Screen name="CharacterComics" component={Comics} options={{headerShown: true}} />
  </Stack.Navigator>
);

export default Routes;
