import 'react-native-gesture-handler';

import React from 'react';
import {View, StatusBar} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';

import colors from '@styles/colors';
import { AuthProvider } from './hooks/auth';
import Routes from './routes';

const App: React.FC = () => (
  <NavigationContainer>
    <StatusBar hidden barStyle="dark-content" backgroundColor={colors.background} />
    <AuthProvider>
      <View style={{flex: 1, backgroundColor: colors.background}}>
        <Routes />
      </View>
    </AuthProvider>
  </NavigationContainer>
);

export default App;
