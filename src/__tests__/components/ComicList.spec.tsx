import React from 'react';
import { render, fireEvent } from 'react-native-testing-library';

import ComicList from '../../components/ComicList';


const comics = [
  { id: 62219, title: 'SPIDER-MAN 2099 VOL. 1', issueNumber: 0, price: 17.99, cover: 'http://i.annihil.us/u/prod/marvel/i/mg/6/90/59c2e3179c62a.jpg' },
  { id: 62220, title: 'SPIDER-MAN 2099 VOL. 2', issueNumber: 1, price: 17.99, cover: 'http://i.annihil.us/u/prod/marvel/i/mg/6/90/59c2e3179c62a.jpg' },
  { id: 62221, title: 'SPIDER-MAN 2099 VOL. 3', issueNumber: 2, price: 17.99, cover: 'http://i.annihil.us/u/prod/marvel/i/mg/6/90/59c2e3179c62a.jpg' },
  { id: 62222, title: 'SPIDER-MAN 2099 VOL. 4', issueNumber: 3, price: 17.99, cover: 'http://i.annihil.us/u/prod/marvel/i/mg/6/90/59c2e3179c62a.jpg' },
  { id: 62223, title: 'SPIDER-MAN 2099 VOL. 5', issueNumber: 4, price: 17.99, cover: 'http://i.annihil.us/u/prod/marvel/i/mg/6/90/59c2e3179c62a.jpg' },
  { id: 62224, title: 'SPIDER-MAN 2099 VOL. 6', issueNumber: 5, price: 17.99, cover: 'http://i.annihil.us/u/prod/marvel/i/mg/6/90/59c2e3179c62a.jpg' },
  { id: 62225, title: 'SPIDER-MAN 2099 VOL. 7', issueNumber: 6, price: 17.99, cover: 'http://i.annihil.us/u/prod/marvel/i/mg/6/90/59c2e3179c62a.jpg' },
  { id: 62226, title: 'SPIDER-MAN 2099 VOL. 8', issueNumber: 7, price: 18.99, cover: 'http://i.annihil.us/u/prod/marvel/i/mg/6/90/59c2e3179c62a.jpg' },
  { id: 62227, title: 'SPIDER-MAN 2099 VOL. 9', issueNumber: 8, price: 20.99, cover: 'http://i.annihil.us/u/prod/marvel/i/mg/6/90/59c2e3179c62a.jpg' },
  { id: 62228, title: 'SPIDER-MAN 2099 VOL. 10', issueNumber: 9, price: 20.99, cover: 'http://i.annihil.us/u/prod/marvel/i/mg/6/90/59c2e3179c62a.jpg' },
  { id: 62229, title: 'SPIDER-MAN 2099 VOL. 11', issueNumber: 10, price: 25.99, cover: 'http://i.annihil.us/u/prod/marvel/i/mg/6/90/59c2e3179c62a.jpg' },
  
];

describe('ComicList Component', () => {
  it('should shows a message if empty', () => {
    const fetchNext = jest.fn();
    const { getByText } = render(<ComicList comics={[]} fetchNextComics={fetchNext} loading={false} />);

    expect(getByText('Esse personagem não contém comics.')).toBeTruthy();
  });

  it('should renders cards', () => {
    const fetchNext = jest.fn();
    const { getAllByTestId } = render(<ComicList comics={comics} fetchNextComics={fetchNext} loading={false} />);

    const cards = getAllByTestId('comic-card');

    expect(cards).toHaveLength(comics.length);
  });

  it('should scrolls to bottom and fetch next comics', () => {
    const fetchNext = jest.fn();
    const { getByTestId } = render(<ComicList comics={comics} fetchNextComics={fetchNext} loading={false} />);

    const flatList = getByTestId('comics-flatlist');
    fireEvent.scroll(flatList, {
      nativeEvent: {
        // Dimensions of the scrollable content
        contentSize: { height: 600, width: 400 },
        contentOffset: { y: 550, x: 0 },
        // Dimensions of the device
        layoutMeasurement: { height: 469, width: 328 }
      }
    })

    expect(fetchNext).toBeCalled();
  });
});