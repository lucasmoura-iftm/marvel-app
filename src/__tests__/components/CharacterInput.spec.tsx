import React from 'react';
import { render, fireEvent } from 'react-native-testing-library';

import CharacterInput from '../../components/CharacterInput';


describe('CharcterInput Component', () => {
  it('should render', () => {
    const value = 'X-men';
    const { getByTestId } = render(<CharacterInput value={value} onClear={() => {}} onSubmit={() => {}} />);
    expect(getByTestId('character-input')).toBeTruthy();
  });

  it('should submit', () => {
    let submitted = false;
    const onSubmit = () => submitted = true;
    const { getByTestId } = render(<CharacterInput value={''} onClear={() => {}} onSubmit={onSubmit} onChangeText={(text) => {}} />);

    const characterInput = getByTestId('character-input');
    fireEvent.changeText(characterInput, 'Wolverine');
    fireEvent(characterInput, 'submitEditing');

    expect(submitted).toBe(true);
  });

  it('should clear input', () => {
    let value = 'X-men';
    const { getByTestId } = render(<CharacterInput value={value} onClear={() => value = ''} onSubmit={() => {}} onChangeText={(text) => value = text} />);
    const characterInput = getByTestId('character-input');

    fireEvent.changeText(characterInput, 'Spider-man');
    expect(value).toBe('Spider-man');

    fireEvent.press(getByTestId('character-input-button'));
    expect(value).toBe('');
  });
});