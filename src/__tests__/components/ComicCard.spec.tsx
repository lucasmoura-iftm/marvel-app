import React from 'react';
import { fireEvent, render} from 'react-native-testing-library';

import ComicCard from '../../components/ComicCard';

jest.mock('@react-navigation/native', () => {
  return {
    ...jest.requireActual('@react-navigation/native'),
    useNavigation: () => ({
      navigate: jest.fn(),
    }),
  };
});

const comic = {
  id: 73921,
  title: 'Spider-Man 2099 Vs. Venom 2099 (Trade Paperback)',
  issueNumber: 0,
  cover: 'http://i.annihil.us/u/prod/marvel/i/mg/1/d0/5caccce2f31c6',
  price: 39.99,
};

describe('ComicCard Component', () => {
  it('should show comic informations', () => {
    const { getByText, findByTestId, getByTestId } = render(<ComicCard comic={comic} index={0} />);
    

    expect(getByText(comic.title)).toBeTruthy();
    expect(getByText('#' + comic.issueNumber)).toBeTruthy();
    expect(getByText('$' + comic.price.toString())).toBeTruthy();
    expect(getByTestId('comic-card-cover')).toBeTruthy();
  });
})