import React from 'react';
import { fireEvent, render} from 'react-native-testing-library';

import CharacterCard from '../../components/CharacterCard';

jest.mock('@react-navigation/native', () => {
  return {
    ...jest.requireActual('@react-navigation/native'),
    useNavigation: () => ({
      navigate: jest.fn(),
    }),
  };
});

const character = {
  id: 1009610,
  name: 'Spider-Man',
  totalComics: 4015,
  thumbnail: 'http://i.annihil.us/u/prod/marvel/i/mg/3/50/526548a343e4b.jpg'
};

describe('CharacterCard Component', () => {
  it('should show character informations', () => {
    const { getByText, findByTestId, getByTestId } = render(<CharacterCard character={character} index={0} />);

    expect(getByText(character.name)).toBeTruthy();
    expect(getByText(character.totalComics.toString())).toBeTruthy();
    expect(getByTestId('character-card-cover')).toBeTruthy();
  });
})