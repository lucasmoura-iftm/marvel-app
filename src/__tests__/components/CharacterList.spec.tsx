import React from 'react';
import { render, fireEvent } from 'react-native-testing-library';

import CharacterList from '../../components/CharacterList';

const characters = [
  { id: 1009610, name: 'Spider-Man', totalComics: 4015, thumbnail: 'http://i.annihil.us/u/prod/marvel/i/mg/3/50/526548a343e4b.jpg' },
  { id: 1011054, name: 'Spider-Man (1602)', totalComics: 44, thumbnail: 'http://i.annihil.us/u/prod/marvel/i/mg/e/03/5317713c9e746.jpg' },
  { id: 1014873, name: 'Spider-Man (2099)', totalComics: 58, thumbnail: 'http://i.annihil.us/u/prod/marvel/i/mg/8/c0/520d1ad3e543f.jpg' },
  { id: 1016452, name: 'Spider-Man (Ai Apaec)', totalComics: 15, thumbnail: 'http://i.annihil.us/u/prod/marvel/i/mg/c/60/4fce7a4d900f4.jpg' },
  { id: 1014858, name: 'Spider-Man #2', totalComics: 34, thumbnail: 'http://i.annihil.us/u/prod/marvel/i/mg/9/c0/5317717bed6fe.jpg' },
  { id: 1014859, name: 'Spider-Man #3', totalComics: 4, thumbnail: 'http://i.annihil.us/u/prod/marvel/i/mg/9/c0/5317717bed6fe.jpg' },
  { id: 1014860, name: 'Spider-Man #4', totalComics: 17, thumbnail: 'http://i.annihil.us/u/prod/marvel/i/mg/9/c0/5317717bed6fe.jpg' },
  { id: 1014861, name: 'Spider-Man #5', totalComics: 65, thumbnail: 'http://i.annihil.us/u/prod/marvel/i/mg/9/c0/5317717bed6fe.jpg' },
  { id: 1014862, name: 'Spider-Man #6', totalComics: 44, thumbnail: 'http://i.annihil.us/u/prod/marvel/i/mg/9/c0/5317717bed6fe.jpg' },
  { id: 1014863, name: 'Spider-Man #7', totalComics: 79, thumbnail: 'http://i.annihil.us/u/prod/marvel/i/mg/9/c0/5317717bed6fe.jpg' },
];

describe('CharacterList Component', () => {
  it('should shows a message if empty', () => {
    const fetchNext = jest.fn();
    const { getByText } = render(<CharacterList characters={[]} fetchNextCharacters={fetchNext} loading={false} />);

    expect(getByText('Nenhum personagem foi encontrado.')).toBeTruthy();
  });

  it('should renders cards', () => {
    const fetchNext = jest.fn();
    const { getAllByTestId } = render(<CharacterList characters={characters} fetchNextCharacters={fetchNext} loading={false} />);

    const cards = getAllByTestId('character-card');

    expect(cards).toHaveLength(characters.length);
  });

  it('should scrolls to bottom and fetch next characters', () => {
    const fetchNext = jest.fn();
    const { getByTestId, getAllByTestId } = render(<CharacterList characters={characters} fetchNextCharacters={fetchNext} loading={false} />);

    const flatList = getByTestId('characters-flatlist');
    fireEvent.scroll(flatList, {
      nativeEvent: {
        // Dimensions of the scrollable content
        contentSize: { height: 600, width: 400 },
        contentOffset: { y: 550, x: 0 },
        // Dimensions of the device
        layoutMeasurement: { height: 469, width: 328 }
      }
    })

    expect(fetchNext).toBeCalled();
  });
});