import React from 'react';
import { render, waitFor } from 'react-native-testing-library';
import Home from '../../pages/Home';

describe('Home Screen', () => {
  it('should render character input', () => {
    const { getByTestId } = render(<Home />)

    expect(getByTestId('character-input')).toBeTruthy();
  });

  it('should render character list', () => {
    const { getByTestId } = render(<Home />)

    expect(getByTestId('characters-flatlist')).toBeTruthy();
  });

  it('should load default characters cards', async () => {
    const { getAllByTestId } = render(<Home />);
    
    await waitFor(() => {
      const charactersCard = getAllByTestId('character-card');
      expect(charactersCard.length).toBeGreaterThan(0);
    });
  });
});