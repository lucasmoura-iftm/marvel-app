import React from 'react';
import { render, waitFor } from 'react-native-testing-library';
import SplashScreen from '../../pages/Splash';

jest.mock("react-native-video", () => "Video");

const fakeNavigation = {
  navigate: jest.fn((page) => {})
};

describe('Splash Screen', () => {
  it('should show video', () => {
    const { getByTestId } = render(<SplashScreen />);

    expect(getByTestId('background-video')).toBeTruthy();
  });
});
