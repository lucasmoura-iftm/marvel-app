import React from 'react';
import { render, act, waitFor } from 'react-native-testing-library';
import Comics from '../../pages/Comics';

const character = {
  id: 1009610,
  name: 'Spider-Man',
  totalComics: 4015,
  thumbnail: 'http://i.annihil.us/u/prod/marvel/i/mg/3/50/526548a343e4b.jpg'
};

const navigationSetOptions = jest.fn();

/*jest.mock('@react-navigation/native', () => ({
  ...jest.requireActual('@react-navigation/native'),
  useNavigation: () => ({ goBack: jest.fn() }),
  useRoute: () => ({
    params: {
      character: { 
        id: 1009610,
        name: 'Spider-Man',
        totalComics: 4015,
        thumbnail: 'http://i.annihil.us/u/prod/marvel/i/mg/3/50/526548a343e4b.jpg' 
      },
    }
  }),
}));*/

describe('Comics Screen', () => {
  it('should load Comics screen', () => {
    const { getByText } = render(<Comics navigation={{ setOptions: navigationSetOptions } } route={ { params: { character } }} />)

    expect(getByText('Comics')).toBeTruthy();
  });

  it('should define screen title with character name', async () => {
    const { getByTestId, getByText } = render(<Comics navigation={{ setOptions: navigationSetOptions } } route={ { params: { character } }} />)

    await waitFor(() => {
      expect(navigationSetOptions).toHaveBeenCalledWith({title: character.name})
    });
  });

  it('should show total comics', async () => {
    const { getByTestId, getByText } = render(<Comics navigation={{ setOptions: navigationSetOptions } } route={ { params: { character } }} />)

    await waitFor(() => {
      expect(getByText(character.totalComics.toString())).toBeTruthy();
    });
  });

  it('should show Spider-man comics', async () => {
    const { getAllByTestId, getByText } = render(<Comics navigation={{ setOptions: navigationSetOptions } } route={ { params: { character } }} />)

    await waitFor(() => {
      const comicsCard = getAllByTestId('comic-card');
      expect(comicsCard.length).toBeGreaterThan(0);
    });
  });
});