import { renderHook, act } from '@testing-library/react-hooks';
import { useCharacters } from '../../hooks/marvelCharacters';

jest.useFakeTimers();

describe('MarvelCharacters Hook', () => {
  it('should fetch characters without specifying name', async () => {
    const characterName = '';
    const { result, waitForNextUpdate, waitFor } = renderHook(() => useCharacters(characterName));

    await waitForNextUpdate();
    const [characters] = result.current;

    expect(characters.length).toBeGreaterThan(0);
  });

  it('should fetch next characters without specifying name', async () => {
    const characterName = '';
    const { result, waitForNextUpdate } = renderHook(() => useCharacters(characterName));
    await waitForNextUpdate();
    const [initialCharacters, fetchCharacters] = result.current;

    expect(initialCharacters.length).toBeGreaterThan(0);
    fetchCharacters(characterName);
    await waitForNextUpdate();
    const [newCharacters] = result.current;
    expect(newCharacters.length).toBeGreaterThan(initialCharacters.length);
  });

  it('should fetch characters specifying name', async () => {
    const characterName = 'Spider';
    const { result, waitForNextUpdate, waitFor } = renderHook(() => useCharacters(characterName));

    await waitForNextUpdate();
    const [characters] = result.current;

    expect(characters.length).toBeGreaterThan(0);
  });

  it('should fetch next characters specifying name', async () => {
    const characterName = 'Spider';
    const { result, waitForNextUpdate } = renderHook(() => useCharacters(characterName));
    await waitForNextUpdate();
    const [initialCharacters, fetchCharacters] = result.current;

    expect(initialCharacters.length).toBeGreaterThan(0);
    fetchCharacters(characterName);
    await waitForNextUpdate();
    const [newCharacters] = result.current;
    expect(newCharacters.length).toBeGreaterThan(initialCharacters.length);
  });
});
