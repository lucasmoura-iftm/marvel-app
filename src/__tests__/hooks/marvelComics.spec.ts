import { renderHook, act } from '@testing-library/react-hooks';
import { useMarvelComics } from '../../hooks/marvelComics';

jest.useFakeTimers();

const characterId = 1009610;

describe('MarvelComics Hook', () => {
  it('should fetch comics', async () => {
    const { result, waitForNextUpdate } = renderHook(() => useMarvelComics(characterId));

    await waitForNextUpdate();
    const [comics] = result.current;

    expect(comics.length).toBeGreaterThan(0);
  });

  it('should fetch next comics', async () => {
    jest.setTimeout(10000);
    const { result, waitForNextUpdate } = renderHook(() => useMarvelComics(characterId));
    await waitForNextUpdate();
    const [initialComics, fetchComics] = result.current;

    expect(initialComics.length).toBeGreaterThan(0);
    act(() => {
      fetchComics();
    });
    await waitForNextUpdate();
    const [newComics] = result.current;
    expect(newComics.length).toBeGreaterThan(initialComics.length);
  });
});
