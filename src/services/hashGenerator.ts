import md5 from 'js-md5';

interface ApiHash {
  ts: number;
  hash: string;
}

const generateTimestamp = () => {
  return Number(new Date());
}

const generateApiHash = (publicApyKey: string, privateApiKey: string): ApiHash => {
  const ts = generateTimestamp();
  const hash = md5.create()
  hash.update(ts + privateApiKey + publicApyKey);
  console.log(ts, hash.hex());
  return { ts, hash: hash.hex() };
}

export default generateApiHash;