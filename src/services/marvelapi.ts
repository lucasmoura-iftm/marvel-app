import axios from 'axios';
import generateApiHash from './hashGenerator';

import { PUBLIC_API_KEY, PRIVATE_API_KEY } from '../config/marvel-api';

export const marvelApi = axios.create({
  baseURL: 'https://gateway.marvel.com:443/v1/public/',
  params: {
    apikey: PUBLIC_API_KEY
  }
});

const fetchMarvelApi = async (endpoint: string, offset: number, apiParams: any) => {
  const apiHash = generateApiHash(PUBLIC_API_KEY, PRIVATE_API_KEY);
  const limit = 10;
  const params = {
      ts: apiHash.ts,
      hash: apiHash.hash,
      limit,
      offset,
      ...apiParams
  };
  console.log('params', params);
  const response = await marvelApi.get(endpoint, { params }).catch(error => console.log(error));
  return response.data;
}

/*export const getComics = async (offset: number, characterName: string) => {
  const apiHash = generateApiHash(PUBLIC_API_KEY, PRIVATE_API_KEY);
  const limit = 20;
  // const offset = (page - 1) * limit;
  const params = {
      ts: apiHash.ts,
      hash: apiHash.hash,
      characters: '1014873',
      limit,
      offset
  };
  const response = await marvelApi.get('comics', { params });
  return response.data;
}*/

type ComicsParameters = {
  orderBy: string;
  characters: string;
}

export const getComics = async (offset: number, characterId: number) => {
  let comicsParams: ComicsParameters = { orderBy: '-onsaleDate', characters: characterId.toString() };

  if (characterId !== null && characterId !== undefined) {
    return await fetchMarvelApi('comics', offset, comicsParams);
  }

  throw new Error("Invalid character id");
}

type CharacterParameters = {
  orderBy: string;
  nameStartsWith?: string;
}

export const getCharacters = async (offset: number, characterName: string = '') => {
  let characterParams: CharacterParameters = { 'orderBy': '-modified' };
  
  if (characterName.length > 0) {
      characterParams['nameStartsWith'] = characterName;
  }

  return await fetchMarvelApi('characters', offset, characterParams);
}
