import colors from '@styles/colors';
import styled from 'styled-components/native';


export const Container = styled.View`
  flex: 1;
  align-items: flex-start;
`;

export const Header = styled.View`
  height: 250px;
  width: 100%;
  background: red;
  justify-content: center;
  align-items: center;
`;

export const CharacterBackground = styled.Image``;

export const CharacterImage = styled.Image`
  width: 150px;
  height: 150px;
  border-radius: 10px;
  border-color: #000;
  border-width: 2px;
  margin-top: 10px;
`;

export const Divider = styled.View`
  width: 100%;
  height: 10px;
  background: ${colors.background};
  border-top-left-radius: 10px;
  border-top-right-radius: 10px;
  position: absolute;
  bottom: 0;
`;


export const ComicsContent = styled.View`
  width: 100%;
  padding: 10px 30px;
  height: 492px;
`;

export const ComicsHeader = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 20px;
`;

export const HeaderTitle = styled.Text`
  color: ${colors.white};
  font-size: 30px;
  font-family: 'Roboto-Bold';
`;

export const TotalContent = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const Total = styled.Text`
  color: ${colors.white};
  margin-left: 10px;
  font-size: 20px;
`;


